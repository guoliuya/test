package com.moreidols.presenter.contract;

import com.moreidols.base.BasePresenter;
import com.moreidols.base.BaseView;
import com.moreidols.model.bean.VideoRes;

/**
 * Description: ClassificationContract
 * Creator: yxc
 * date: 2016/9/21 17:55
 */
public interface ClassificationContract {

    interface View extends BaseView<Presenter> {

        boolean isActive();

        void showContent(VideoRes videoRes);

        void refreshFaild(String msg);
    }

    interface Presenter extends BasePresenter {
        void onRefresh();
    }
}
