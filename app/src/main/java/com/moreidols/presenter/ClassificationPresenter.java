package com.moreidols.presenter;

import android.support.annotation.NonNull;

import rx.Subscription;
import rx.functions.Action1;

import com.moreidols.base.RxPresenter;
import com.moreidols.model.bean.VideoRes;
import com.moreidols.model.exception.ApiException;
import com.moreidols.model.net.HttpMethods;
import com.moreidols.model.net.MyObserver;
import com.moreidols.presenter.contract.ClassificationContract;
import com.moreidols.utils.Preconditions;

/**
 * Description: ClassificationPresenter
 * Creator: yxc
 * date: 2016/9/21 17:55
 */
public class ClassificationPresenter extends RxPresenter implements ClassificationContract.Presenter {
    ClassificationContract.View mView;
    int page = 0;

    public ClassificationPresenter(@NonNull ClassificationContract.View twoView) {
        mView = Preconditions.checkNotNull(twoView);
        mView.setPresenter(this);
    }

    @Override
    public void onRefresh() {
        page = 0;
        getPageHomeInfo();
    }

    private void getPageHomeInfo() {
        HttpMethods.getInstance().queryClassification()
                .subscribe(new MyObserver<VideoRes>() {
                    @Override
                    protected void onError(ApiException ex) {
                        mView.refreshFaild(ex.getDisplayMessage());
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onNext(VideoRes res) {
                        if (res != null) {
                            if (mView.isActive()) {
                                mView.showContent(res);
                            }
                        }
                    }
                });
    }
}
