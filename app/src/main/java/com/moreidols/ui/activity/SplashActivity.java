package com.moreidols.ui.activity;

import java.util.concurrent.TimeUnit;

import android.content.Intent;
import android.os.Bundle;

import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

import com.moreidols.base.BaseActivity;
import com.moreidols.me.R;
import com.moreidols.utils.RxUtil;

/**
 * Created by guoliuya on 2017/11/24.
 */

public class SplashActivity extends BaseActivity {
    private static final int COUNT_DOWN_TIME = 1000;
    protected CompositeSubscription mCompositeSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        unbinder = ButterKnife.bind(this);
        startCountDown();

    }

    private void startCountDown() {
        Subscription rxSubscription = Observable.timer(COUNT_DOWN_TIME, TimeUnit.MILLISECONDS)
                .compose(RxUtil.<Long>rxSchedulerHelper())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        startActivity(new Intent(SplashActivity.this,MainActivity.class));
                    }
                });
        addSubscribe(rxSubscription);
    }

    protected void addSubscribe(Subscription subscription) {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = new CompositeSubscription();
        }
        mCompositeSubscription.add(subscription);
    }
}
