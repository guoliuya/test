package com.moreidols.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.moreidols.base.App;
import com.moreidols.base.BaseActivity;
import com.moreidols.me.R;
import com.moreidols.utils.EventUtil;

public class MainActivity extends BaseActivity {

    private Long firstTime = 0L;

    @BindView(R.id.app_tv)
    Button tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
    }

    @OnClick({ R.id.app_tv })
    public void onclick(View view) {
       switch (view.getId()){
           case R.id.app_tv:
              startActivity(new Intent(MainActivity.this,DetailInfoActivity.class));
               break;
       }
    }

    @Override
    public void onBackPressed() {
            long secondTime = System.currentTimeMillis();
            if (secondTime - firstTime > 1500) {
                EventUtil.showToast(this, "再按一次退出");
                firstTime = secondTime;
            } else {
                App.getInstance().exitApp();
            }
    }

}
