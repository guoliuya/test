package com.moreidols.ui.activity;

import android.widget.TextView;

import butterknife.BindView;

import com.moreidols.base.SwipBackActivity;
import com.moreidols.me.R;

/**
 * Created by guoliuya on 2017/11/24.
 */

public class DetailInfoActivity extends SwipBackActivity{

    @BindView(R.id.test_tv)
    TextView tv;

    @Override
    protected int setLayoutResourceID() {
        return R.layout.activity_detail;
    }

    @Override
    protected void onBaseCreate() {
    }
}
