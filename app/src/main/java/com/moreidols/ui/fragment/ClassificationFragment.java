package com.moreidols.ui.fragment;

import android.view.LayoutInflater;
import android.widget.ImageView;

import butterknife.BindView;
import org.json.JSONObject;

import com.moreidols.base.BaseFragment;
import com.moreidols.me.R;
import com.moreidols.presenter.ClassificationPresenter;
import com.moreidols.ui.view.ClassificationView;

public class ClassificationFragment extends BaseFragment {
    private ClassificationPresenter mPresenter;

    @BindView(R.id.two_view)
    ClassificationView twoView;

    @Override
    protected int getLayout() {
        return R.layout.fragment_classification;
    }

    @Override
    protected void initView(LayoutInflater inflater) {
        mPresenter = new ClassificationPresenter(twoView);
    }

    @Override
    protected void lazyFetchData() {
        ((ClassificationPresenter) mPresenter).onRefresh();
    }

    private void getPageHomeInfo() {
//        String postParams = getPostParams(getPostParamsData());
//        Call<ResponseBody> friendData = RetrofitHelper.getVideoApi().getFriendData(postParams);
//        friendData.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call,
//                    retrofit2.Response<ResponseBody> response) {
//                Logger.d("guoliuya",response.body().toString());
//
//
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//            }
//        });
    }

    private String getPostParams(String data) {
        JSONObject json = new JSONObject();
        try {
            json.put("code", "0000");
            json.put("osType", "android");
            json.put("appId", "com.xygame.sg");
            json.put("userId", "");
            json.put("timeStamp", System.currentTimeMillis());
            json.put("device","");
            json.put("data", data);
            json.put("channel", "");
            json.put("version", "");
            return json.toString();
        } catch (Exception e) {
            return "";
        }
    }

    private String getPostParamsData() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("page", 1);
            obj.put("size", 20);
            // 设置经纬度
            obj.put("lng","");
            obj.put("lat", "");
            obj.put("lastTime", "");
            return obj.toString();
        } catch (Exception e) {
            return "";
        }
    }
}
