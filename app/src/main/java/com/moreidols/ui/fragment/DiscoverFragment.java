package com.moreidols.ui.fragment;

import android.view.LayoutInflater;
import android.widget.ImageView;

import butterknife.BindView;

import com.moreidols.base.BaseFragment;
import com.moreidols.me.R;

public class DiscoverFragment extends BaseFragment {

    @BindView(R.id.one_view)
    ImageView threeView;

    @Override
    protected int getLayout() {
        return R.layout.fragment_discovery;
    }

    @Override
    protected void initView(LayoutInflater inflater) {
    }

    @Override
    protected void lazyFetchData() {
    }
}
