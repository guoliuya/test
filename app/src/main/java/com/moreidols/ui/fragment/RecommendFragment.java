package com.moreidols.ui.fragment;

import android.view.LayoutInflater;
import android.widget.ImageView;

import butterknife.BindView;

import com.moreidols.base.BaseFragment;
import com.moreidols.me.R;

public class RecommendFragment extends BaseFragment {

    @BindView(R.id.three_view)
    ImageView oneView;

    @Override
    protected int getLayout() {
        return R.layout.fragment_recommend;
    }

    @Override
    protected void initView(LayoutInflater inflater) {
    }

    @Override
    protected void lazyFetchData() {
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
