package com.moreidols.ui.fragment;

import android.view.LayoutInflater;

import com.moreidols.base.BaseFragment;
import com.moreidols.me.R;

public class MineFragment extends BaseFragment {
    public static final String SET_THEME = "SET_THEME";

    @Override
    protected int getLayout() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initView(LayoutInflater inflater) {
        super.initView(inflater);
    }
}
