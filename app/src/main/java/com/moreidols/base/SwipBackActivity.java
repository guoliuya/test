package com.moreidols.base;

import android.os.Bundle;
import android.view.LayoutInflater;

import butterknife.ButterKnife;

import com.moreidols.me.R;
import com.moreidols.ui.view.SwipeBackLayout;

/**
 * Created by guoliuya on 2017/11/24.
 */

public abstract class SwipBackActivity extends BaseActivity{

    protected SwipeBackLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayoutResourceID());
        layout = (SwipeBackLayout) LayoutInflater.from(this).inflate(
                R.layout.base, null);
        layout.attachToActivity(this);
        unbinder = ButterKnife.bind(this);
        onBaseCreate();
    }
    protected abstract int setLayoutResourceID();
    protected abstract void onBaseCreate();
}
